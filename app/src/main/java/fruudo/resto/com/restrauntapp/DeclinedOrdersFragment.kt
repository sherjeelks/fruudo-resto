package fruudo.resto.com.restrauntapp


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_declined_orders.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DeclinedOrdersFragment : Fragment() {

    var orders = ArrayList<Order>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_declined_orders, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        recyclerView.layoutManager = LinearLayoutManager(context)
        //recyclerView.adapter

        getOrders()

    }

    fun getOrders()
    {
        val url = "https://fruudo.com/merchantapp/api/searchOrder?token="+MyUtil.getPref(context,"token")+"&mtid="+MyUtil.getPref(context,"merchant_id")+"&user_type=admin"

        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()

                  }

                is Result.Success -> {

                    val data = result.get()

                    Log.e("Decline","Orders resp "+data)

                    val obj = JSONObject(data)

                    if (obj.getInt("code")==1)
                    {
                        val arr = obj.getJSONArray("details")
                        val gson = Gson()
                        val collectionType = object : TypeToken<List<Order>>() {}.type

                        val parser = SimpleDateFormat("MMM dd,yyyy HH:mm:ss")

                        for (i in 0 until arr.length()) {

                            val jObj = arr.getJSONObject(i)
                            if (DateUtils.isToday(parser.parse(jObj.getString("delivery_date")).time) && jObj.getString("status_raw")=="decline")
                            {
                                orders.add(gson.fromJson(jObj.toString(), Order::class.java))
                            }
                        }

                    }else
                    {
                        Toast.makeText(context,"Something is not right!", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }

    }


}
