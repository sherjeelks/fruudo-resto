package fruudo.resto.com.restrauntapp


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_today_order.*
import org.json.JSONObject
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class TodayOrderFragment : Fragment() {

    var orders = ArrayList<Order>()
    lateinit var adapter:OrdersAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_today_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = OrdersAdapter(context,orders)
        recyclerView.adapter = adapter
        getOrders()
    }

    private fun getOrders()
    {
        val url = "https://fruudo.com/merchantapp/api/GetTodaysOrder?token="+MyUtil.getPref(context,"token")+"&mtid="+MyUtil.getPref(context,"merchant_id")+"&user_type=admin"
        Log.d("URL","URL "+url)
        loading_layout.visibility = View.VISIBLE
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    message.setText("Connection issue occurred")
                    image.setImageResource(R.drawable.broken_glass)
                }

                is Result.Success -> {

                    val data = result.get()

                    Log.e("Today","Orders resp "+data)

                    val obj = JSONObject(data)

                    if (obj.getInt("code")==1)
                    {
                        val arr = obj.getJSONArray("details")
                        val gson = Gson()
                        val collectionType = object : TypeToken<List<Order>>() {}.type

                        var mOrders = ArrayList<Order>()
                        mOrders = gson.fromJson(arr.toString(), collectionType)
                        for (item in mOrders) if(item.status.toLowerCase()=="paid" || item.status.toLowerCase()=="initial_order")orders.add(item)
                        adapter.notifyDataSetChanged()
                        if (loading_layout!=null)
                        loading_layout.visibility = View.GONE

                    }else
                    {
                        if(context!=null)
                        Toast.makeText(context,"No orders found!", Toast.LENGTH_SHORT).show()
                        message.setText("No orders found!")
                        progress.visibility = View.GONE
                        image.setImageResource(R.drawable.empty_red_box)
                    }

                }
            }
        }

    }


}
