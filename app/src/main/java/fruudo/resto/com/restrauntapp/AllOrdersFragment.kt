package fruudo.resto.com.restrauntapp


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_all_orders.*
import org.json.JSONObject
import java.util.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AllOrdersFragment : Fragment() {

    var orders = ArrayList<OrderHistory>()
    lateinit var adapter:OrdersHistoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all_orders, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = OrdersHistoryAdapter(context,orders)
        recyclerView.adapter = adapter
        getOrders()

    }

    private fun getOrders()
    {
        val url = "https://fruudo.com/merchantapp/api/GetAllOrders?token="+MyUtil.getPref(context,"token")+"&mtid="+MyUtil.getPref(context,"merchant_id")+"&user_type=admin"
        if (loading_layout!=null)
            loading_layout.visibility = View.VISIBLE
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    message.setText("Connection error occurred!")
                    image.setImageResource(R.drawable.broken_glass)
                }

                is Result.Success -> {

                    val data = result.get()

                    Log.e("Today","Orders resp "+data)

                    val obj = JSONObject(data)

                    if (obj.getInt("code")==1)
                    {
                        val arr = obj.getJSONArray("details")
                        val gson = Gson()
                        val collectionType = object : TypeToken<List<OrderHistory   >>() {}.type

                        var mOrders = ArrayList<OrderHistory>()
                        mOrders = gson.fromJson(arr.toString(), collectionType)
                        for (item in mOrders) orders.add(item)
                        adapter.notifyDataSetChanged()
                        if (loading_layout!=null)
                        loading_layout.visibility = View.GONE

                    }else
                    {
                        Toast.makeText(context,"No orders found!", Toast.LENGTH_SHORT).show()
                        message.setText("No orders found!")
                        progress.visibility = View.GONE
                        image.setImageResource(R.drawable.empty_red_box)
                    }

                }
            }
        }

    }

}
