package fruudo.resto.com.restrauntapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kotlin.jvm.internal.Intrinsics;

public class MyUtil {


    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static void startNewMainActivity(Activity currentActivity, Class<? extends Activity> newTopActivityClass, String key, String value) {
        Intent intent = new Intent(currentActivity, newTopActivityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(key,value);
        currentActivity.startActivity(intent);
    }


    @Nullable
    public static Address getAddress(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        try {
            List var10000 = geocoder.getFromLocation(latitude, longitude, 1);
            Intrinsics.checkExpressionValueIsNotNull(var10000, "geocoder.getFromLocation(latitude, longitude, 1)");
            return (Address) var10000.get(0);
        } catch (IOException var8) {
            var8.printStackTrace();
            return null;
        }
    }


    public static void savePref(Context context, String key, String value)
    {

        SharedPreferences.Editor editor = context.getSharedPreferences("APP", Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();

    }


    public static String getPref(Context context, String key)
    {

        SharedPreferences prefs = context.getSharedPreferences("APP", Context.MODE_PRIVATE);
        return  prefs.getString(key, "na");


    }


    public static void savePref(Context context, String key, boolean value)
    {

        SharedPreferences.Editor editor = context.getSharedPreferences("APP", Context.MODE_PRIVATE).edit();
        editor.putBoolean(key, value);
        editor.apply();

    }


    public static boolean getBoolPref(Context context, String key)
    {

        SharedPreferences prefs = context.getSharedPreferences("APP", Context.MODE_PRIVATE);
        return  prefs.getBoolean(key, false);


    }


    public static String correctResponse(String response)
    {
        if (response.startsWith("("))
        {
            response = response.substring(1);
            response = response.substring(0,response.length()-1);
        }
        response = response.replaceAll("&euro; ","€");


        return response;
    }

    public static boolean hasItem(JSONArray array, String key, String value)
    {
        for (int i = 0; i < array.length(); i++) {

            try {
                if (array.getJSONObject(i).has(key))
                {
                    if (array.getJSONObject(i).getString(key).equals(value))
                        return true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        return false;
    }





    public static JSONObject getItem(JSONArray array, String key, String value)
    {
        for (int i = 0; i < array.length(); i++) {

            try {
                if (array.getJSONObject(i).has(key))
                {
                    if (array.getJSONObject(i).getString(key).equals(value))
                        return array.getJSONObject(i);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        return null;
    }

    public static String convertDate(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }

    public static JSONArray removeItem(JSONArray array, String key, String value)
    {
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < array.length(); i++) {

            try {
                if (array.getJSONObject(i).has(key))
                {
                    if (array.getJSONObject(i).getString(key).equals(value))
                    {
                       Log.d("FOUND","IGNORE THIS ITEM, THIS HAS TO BE REMOVED "+array.getJSONObject(i));
                    }else
                    {
                        jsonArray.put(array.getJSONObject(i));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        return jsonArray;
    }


    public static void animator(final View view)
    {
        if (view.getVisibility()!=View.VISIBLE)
            view.setVisibility(View.VISIBLE);

        TranslateAnimation anim = new TranslateAnimation(0,0,0, view.getHeight());
        anim.setDuration(300);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                Log.e("END","ANIMATION END ");
                view.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }



    public static void animatorUp(final View view)
    {

        TranslateAnimation anim = new TranslateAnimation(0,0,view.getHeight(), 0);
        anim.setDuration(300);
        view.setVisibility(View.VISIBLE);
        view.startAnimation(anim);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {



            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }


    public static boolean hasKey(JSONArray  arr, String key)
    {
        try {

            for (int i = 0; i < arr.length(); i++) {

                if (arr.getString(i).equals(key))
                    return true;

            }

        }catch (Exception e){

            e.printStackTrace();

        }


        return false;
    }


    public static JSONArray remove(JSONArray  arr, String key)
    {
        ArrayList<String> list = new ArrayList<String>();

        int len = arr.length();

        try {

            if (arr != null) {
                for (int i=0;i<len;i++){
                    if (!arr.getString(i).equals(key))
                    list.add(arr.get(i).toString());
                }
            }


        }catch (Exception e){

            e.printStackTrace();

        }

        return new JSONArray(list);
    }

}
