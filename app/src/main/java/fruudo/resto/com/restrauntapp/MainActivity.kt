package fruudo.resto.com.restrauntapp

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Handler
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_login.*
import org.json.JSONObject
import com.google.gson.reflect.TypeToken



class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_email).text = MyUtil.getPref(this, "username")

        nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_header_name).text = MyUtil.getPref(this, "restaurant_name")

        Glide.with(this).load(MyUtil.getPref(this,"logo")).into(nav_view.getHeaderView(0).findViewById<ImageView>(R.id.banner))

        Log.e("LOGO","LGOO "+MyUtil.getPref(this,"logo"))

        supportFragmentManager.inTransaction {
            replace(R.id.frame, TodayOrderFragment())
        }
        nav_view.setCheckedItem(R.id.nav_today_order)
        supportActionBar?.setTitle("New Orders")

        if (!MyUtil.getBoolPref(this,"token_saved") && MyUtil.getPref(this,"push_token")!="na")
        {
            //save token
            saveToken(MyUtil.getPref(this,"push_token"))
        }

        Log.d("Lat","Long "+MyUtil.getPref(this,"lat")+" lng "+MyUtil.getPref(this,"lng"))
    }

    internal fun saveToken(token: String) {
        Fuel.get("http://fruudo.com/merchantapp/api/UpdatePush?push_token=" + token + "&merchant_id=" + MyUtil.getPref(applicationContext, "merchant_id")).responseString(object : Handler<String> {
            override fun failure(request: Request, response: Response, error: FuelError) {
                //do something when it is failure
                error.printStackTrace()
            }

            override fun success(request: Request, response: Response, data: String) {
                //do something when it is successful
                MyUtil.savePref(applicationContext, "token_saved", true)

            }
        })
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_today_order -> {
                // Handle the camera action
                supportFragmentManager.inTransaction {
                    replace(R.id.frame, TodayOrderFragment())
                }
                supportActionBar?.setTitle("Today's Orders")
            }
            R.id.nav_all_orders -> {

                supportFragmentManager.inTransaction {
                    replace(R.id.frame, AllOrdersFragment())
                }
                supportActionBar?.setTitle("All Orders")


            }
            R.id.nav_ready_orders -> {

                supportFragmentManager.inTransaction {
                    replace(R.id.frame, ReadyDeliveriesFragment())
                }
                supportActionBar?.setTitle("Ready Orders")

            }

            R.id.nav_accepted_orders -> {

                supportFragmentManager.inTransaction {
                    replace(R.id.frame, AcceptedOrders())
                }
                supportActionBar?.setTitle("Accepted Orders")

            }
            R.id.nav_logout -> {

                MyUtil.savePref(this,"token","na")
                MyUtil.savePref(this,"username","na")
                MyUtil.savePref(this,"login",false)
                MyUtil.savePref(this,"restaurant_name","na")
                MyUtil.savePref(this,"user_type","na")
                MyUtil.savePref(this,"merchant_id","na")
                MyUtil.savePref(this,"token_saved",false)
                startActivity(Intent(this,LoginActivity::class.java))
                finish()

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.func()
        fragmentTransaction.commit()
    }

}
