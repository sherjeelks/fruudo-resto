package fruudo.resto.com.restrauntapp

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.order_layout.view.*
import java.util.ArrayList


class OrdersAdapter (val context: Context?, val orders: ArrayList<Order>) : RecyclerView.Adapter<OrdersAdapter.OrdersAdapterViewHolder>() {



    override fun getItemCount(): Int {
        return orders.size
    }

    override fun onBindViewHolder(holder: OrdersAdapterViewHolder, position: Int) {

        holder.bind(position,orders.get(position))

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdersAdapterViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.order_layout, parent, false)
        return OrdersAdapterViewHolder(itemView)

    }

    inner class OrdersAdapterViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        fun bind(pos:Int, order: Order) = with(itemView) {

            type.setText(order.trans_type.capitalize())
            orderId.setText("Order Id #"+order.order_id)
            if (order.items!=null && order.items.size>1)
                dishName.setText(order.items[0].item_name+" and "+order.items.size+" More")
            else
                dishName.setText(order.items[0].item_name)


            if (order.status=="accepted")
                order_status.setBackgroundResource(R.drawable.purple_rounded_background)
            else if (order.status=="ready")
                order_status.setBackgroundResource(R.drawable.green_rounded_bg)
            else if (order.status=="cancelled")
                order_status.setBackgroundResource(R.drawable.red_button)
            else if (order.status=="decline")
                order_status.setBackgroundResource(R.drawable.red_button)


            order_status.setText(order.status.capitalize())
            date.setText(order.transaction_date)
            total_value.setText(MyUtil.correctResponse(order.total_w_tax_prety))


            card.setOnClickListener {

                context.startActivity(Intent(context,OrderDetailActivity::class.java).putExtra("order_id",order.order_id))

            }

        }


    }

}

