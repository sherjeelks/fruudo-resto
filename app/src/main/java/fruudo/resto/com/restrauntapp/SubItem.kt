package fruudo.resto.com.restrauntapp

data class SubItem(
        val addon_name: String,
        val addon_category: String,
        val addon_qty: String,
        val addon_price: String
)