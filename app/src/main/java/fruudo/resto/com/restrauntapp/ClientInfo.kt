package fruudo.resto.com.restrauntapp

data class ClientInfo(
        val full_name: String,
        val client_id: String,
        val email_address: String,
        val address: String,
        val location_name: String,
        val contact_phone: String,
        val delivery_lat: Double,
        val delivery_lng: Double
)