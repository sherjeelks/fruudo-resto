package fruudo.resto.com.restrauntapp

data class Total(
        val subtotal: String,
        val taxable_total: String,
        val delivery_charges: String,
        val total: String,
        val tax: String,
        val tax_amt: String,
        val curr: String,
        val mid: String,
        val discounted_amount: Int,
        val merchant_discount_amount: Int,
        val merchant_packaging_charge: Int,
        val less_voucher: Int,
        val voucher_type: String,
        val tips: String,
        val tips_percent: String,
        val pts_redeem_amt: Int,
        val subtotal1: String,
        val subtotal2: String
)