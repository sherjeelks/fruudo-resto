package fruudo.resto.com.restrauntapp

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.product_layout.view.*
import java.util.ArrayList


class ProductsAdapter (val context: Context?, val products: List<ItemX>) : RecyclerView.Adapter<ProductsAdapter.ProductsAdapterViewHolder>() {



    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ProductsAdapterViewHolder, position: Int) {

        holder.bind(position,products.get(position))

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsAdapterViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.product_layout, parent, false)
        return ProductsAdapterViewHolder(itemView)

    }

    inner class ProductsAdapterViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        fun bind(pos:Int, product: ItemX) = with(itemView) {

            dishName.text = product.item_name
            size.text = product.size_words.capitalize()
            quntity.text = product.qty
            total.text = product.total_price

            if(product.sub_item!=null && product.sub_item.size>0)
            {
                for (item in product.sub_item)
                {
                    val textView = TextView(context)
                    textView.setPadding(15,15,15,15)
                    textView.text = item.addon_name+ " X "+item.addon_qty
                    addonsList.addView(textView)
                }
            }


            if (product.cooking_ref.isEmpty())
                notes.visibility = View.GONE
            else
            {
                notes.visibility = View.VISIBLE
                notes.text = "Instructions : "+product.cooking_ref
            }

        }


    }

}