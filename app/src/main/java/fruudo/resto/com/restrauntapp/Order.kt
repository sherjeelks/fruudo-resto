package fruudo.resto.com.restrauntapp


data class Order(
        val order_id: String,
        val viewed: String,
        val status_raw: String,
        val status: String,
        val trans_type_raw: String,
        val trans_type: String,
        val total_w_tax: String,
        val total_w_tax_prety: String,
        val items: List<Item>,
        val transaction_date: String,
        val transaction_time: String,
        val delivery_time: Boolean,
        val delivery_asap: String,
        val delivery_date: Any,
        val customer_name: String
)