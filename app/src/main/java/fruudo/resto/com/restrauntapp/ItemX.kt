package fruudo.resto.com.restrauntapp

data class ItemX(
        val item_id: String,
        val item_name: String,
        val size_words: String,
        val qty: String,
        val sub_item: List<SubItem>,
        val normal_price: String,
        val discounted_price: String,
        val order_notes: String,
        val cooking_ref: String,
        val ingredients: String,
        val non_taxable: String,
        val total_price: String
)