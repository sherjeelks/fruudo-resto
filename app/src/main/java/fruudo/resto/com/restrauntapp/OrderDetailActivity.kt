package fruudo.resto.com.restrauntapp

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.android.synthetic.main.activity_order_detail.*
import kotlinx.android.synthetic.main.content_order_detail.*
import kotlinx.android.synthetic.main.order_layout.view.*
import org.json.JSONObject
import java.util.ArrayList
import android.support.v4.content.LocalBroadcastManager
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter



class OrderDetailActivity : AppCompatActivity() {

    val drivers = ArrayList<String>()
    var current = 0
    var handler: Handler? = null
    private  var clientId = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Order #"+intent.getStringExtra("order_id")
        getDetails()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId==android.R.id.home)
        {
            onBackPressed()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun getDetails() {
        val url = "https://fruudo.com/merchantapp/api/OrderdDetails?token=" + MyUtil.getPref(this, "token") + "&mtid=" + MyUtil.getPref(this, "merchant_id") + "&user_type=admin&order_id="+intent.getStringExtra("order_id")
        Log.e("URL","URL IS "+url)
        loading_layout.visibility = View.VISIBLE
        message.text = "Please wait loading order details..."

        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    message.setText("There is some connection issue!")
                }

                is Result.Success -> {

                    val data = result.get()

                    Log.e("Today", "Orders resp " + data)

                    val obj = JSONObject(MyUtil.correctResponse(data))

                    if (obj.getInt("code") == 1) {
                        val details = obj.getJSONObject("details")
                        val gson = Gson()

                        val orderDetails = gson.fromJson(details.toString(), OrderDetailsX::class.java)
                        clientId = orderDetails.client_info.client_id
                        setUI(orderDetails)

                    } else {
                        Toast.makeText(this, "Something is not right!", Toast.LENGTH_SHORT).show()
                        message.setText("Looks like something is wrong with this order!")
                    }

                }
            }
        }

    }

    fun setUI(orderDetails:OrderDetailsX)
    {

        delivery_date.text = orderDetails.delivery_date
        if (orderDetails.delivery_time.equals("false"))
            delivery_time.text = "ASAP"
        else
        delivery_time.text = orderDetails.delivery_time
        status.text = "Current Status : "+orderDetails.status.capitalize()
        customer_name.text = orderDetails.client_info.full_name
        subtotal.text = orderDetails.total.subtotal2
        tax.text = orderDetails.total.taxable_total
        total.text = orderDetails.total.total
        address.text = orderDetails.client_info.address

        timestamp.text = "Order arrived at : "+orderDetails.transaction_date

        if(orderDetails.driver_info.none==1)
        {
            //No driver info hide it
            driver_card.visibility = View.GONE
        }else
        {
            driver_card.visibility = View.VISIBLE
            driver_transport_type.text = orderDetails.driver_info.transport_type_id.capitalize()
            driver_phone.text = orderDetails.driver_info.phone
            driver_name.text = orderDetails.driver_info.first_name.capitalize()+" "+orderDetails.driver_info.last_name.capitalize()
            license_plate.text = orderDetails.driver_info.licence_plate
        }

        if (orderDetails.trans_type_raw=="pickup")
        {
            date_label.text = "Pickup Date : "
            time_label.text = "Pickup Time : "
        }

        orderType.text = orderDetails.trans_type.capitalize()

        if (!orderDetails.delivery_instruction.isEmpty())
        notes.text = orderDetails.delivery_instruction
        else
            notes_card.visibility = View.GONE

        if (orderDetails.status=="accepted")
            status.setBackgroundResource(R.color.deep_purple_dark)
        else if (orderDetails.status=="ready")
            status.setBackgroundResource(R.color.green)
        else if (orderDetails.status=="cancelled")
            status.setBackgroundResource(R.color.red)
        else if (orderDetails.status=="decline")
            status.setBackgroundResource(R.color.red)


        phone.text = orderDetails.client_info.contact_phone
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ProductsAdapter(this,orderDetails.item)

        loading_layout.visibility = View.GONE

        if (orderDetails.status_raw=="paid")
        {
            addTime.setText("Accept")
            mark_ready.setText("Decline")

            addTime.visibility = View.VISIBLE
            addTime.setOnClickListener { acceptOrder() }
            mark_ready.setOnClickListener { declineOrder() }

        }else if (orderDetails.status_raw=="accepted")
        {
            addTime.setText("Add Time")
            addTime.visibility = View.GONE

            mark_ready.setText("Mark Ready")
            mark_ready.setOnClickListener { changeOrderStatus("ready") }

        }else
        {
            addTime.visibility = View.GONE
            mark_ready.visibility = View.GONE
        }

    }




    fun acceptOrder()
    {
        val url = "https://fruudo.com/merchantapp/api/AcceptOrdes?token=" + MyUtil.getPref(this, "token") + "&mtid=" + MyUtil.getPref(this, "merchant_id") + "&user_type=admin&order_id="+intent.getStringExtra("order_id")
        loading_layout.visibility = View.VISIBLE
        message.text = "Please wait accepting the order"

        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    message.text = "There is some connection issue!"
                }

                is Result.Success -> {

                    val data = result.get()

                    Log.e("OrderDetails", "Order accept response " + data)

                    val obj = JSONObject(MyUtil.correctResponse(data))

                    if (obj.getInt("code") == 1) {

                        loading_layout.visibility = View.GONE
                        Toast.makeText(this, "Order accepted successfully!", Toast.LENGTH_SHORT).show()
                        sendCustomerPush("accepted","Your order %23"+intent.getStringExtra("order_id")+" is now accepted by restaurant and getting cooked")
                        getDetails()

                    } else {
                        Toast.makeText(this, "Something is not right!", Toast.LENGTH_SHORT).show()
                        message.text = "Looks like something is wrong with this order!"
                    }

                }
            }
        }
    }



    private fun declineOrder()
    {
        val url = "https://fruudo.com/merchantapp/api/DeclineOrders?token=" + MyUtil.getPref(this, "token") + "&mtid=" + MyUtil.getPref(this, "merchant_id") + "&user_type=admin&order_id="+intent.getStringExtra("order_id")
        loading_layout.visibility = View.VISIBLE
        message.text = "Please wait declining the order"

        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    message.text = "There is some connection issue!"
                }

                is Result.Success -> {

                    val data = result.get()

                    Log.e("OrderDetails", "Order is declined " + data)

                    val obj = JSONObject(MyUtil.correctResponse(data))

                    if (obj.getInt("code") == 1) {

                        loading_layout.visibility = View.GONE
                        Toast.makeText(this, "Order declined successfully!", Toast.LENGTH_SHORT).show()
                        sendCustomerPush("declined","Your order %23"+intent.getStringExtra("order_id")+" is declined by restaurant, you can try another restaurant.")
                        getDetails()

                    } else {
                        Toast.makeText(this, "Something is not right!", Toast.LENGTH_SHORT).show()
                        message.text = "Looks like something is wrong with this order!"
                    }

                }
            }
        }
    }


    fun changeOrderStatus(status:String)
    {
        val url = "https://fruudo.com/merchantapp/api/ChangeOrderStatus?token=" + MyUtil.getPref(this, "token") + "&mtid=" + MyUtil.getPref(this, "merchant_id") + "&user_type=admin&order_id="+intent.getStringExtra("order_id")+"&status="+status
        loading_layout.visibility = View.VISIBLE
        message.text = "Please wait moving order to "+status+" queue"

        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    message.text = "There is some connection issue!"
                }

                is Result.Success -> {

                    val data = result.get()

                    Log.e("Today", "Orders resp " + data)

                    val obj = JSONObject(MyUtil.correctResponse(data))

                    if (obj.getInt("code") == 1) {

                        loading_layout.visibility = View.GONE
                        Toast.makeText(this, "Order moved to "+status+" successfully!", Toast.LENGTH_SHORT).show()
                        getDetails()

                        if (status=="ready")
                        {
                            //Find Drivers
                            sendCustomerPush("ready","Your order %23"+intent.getStringExtra("order_id")+" is now ready to pickup by rider.")
                            findDrivers()
                        }

                    } else {
                        Toast.makeText(this, "Something is not right!", Toast.LENGTH_SHORT).show()
                        message.text = "Looks like something is wrong with this order!"
                    }

                }
            }
        }
    }


   private fun findDrivers()
   {
       val url = "http://fruudo.com/merchantapp/api/GetClosestDriver?lat="+MyUtil.getPref(this,"lat")+"&lng="+MyUtil.getPref(this,"lng")
        loading_layout.visibility = View.VISIBLE
        image.setImageResource(R.drawable.food_delivery)
        message.setText("Please wait finding riders in our area...")

       url.httpGet().responseString { request, response, result ->
           //do something with response
           when (result) {
               is Result.Failure -> {
                   val ex = result.getException()
                   ex.printStackTrace()
                   loading_layout.visibility = View.GONE
                   Toast.makeText(this,"Network error occurred",Toast.LENGTH_SHORT).show()
               }

               is Result.Success -> {

                   val data = result.get()

                   Log.d("Today", "Orders resp " + data)

                   val obj = JSONObject(MyUtil.correctResponse(data))

                   if (obj.getInt("code") == 1) {

                       val array = obj.getJSONArray("details")

                       for (i in 0 until array.length())
                       {
                           if (array.getJSONObject(i).getString("on_duty")== "1")
                           {
                               Log.d("Driver","Driver "+array.getJSONObject(i).getString("driver_id"))
                               drivers.add(array.getJSONObject(i).getString("driver_id"))
                           }
                       }

                       Log.d("Driver","Size of the driver is "+drivers.size)

                        scheduleDelivery()

                   } else {
                       Toast.makeText(this, "Something is not right!", Toast.LENGTH_SHORT).show()
                   }

               }
           }
       }

   }

    private fun scheduleDelivery()
    {

        if (current<drivers.size)
        {
            sendPushToDriver(drivers[current])
        }else
        {
            //NO drivers found or all done
            message.text = "No drivers found in your area"
            retry_button.visibility = View.VISIBLE
            progress.visibility = View.GONE

        }

    }


    fun sendPushToDriver(driverId:String)
    {
        current++
        val url = "http://fruudo.com/merchantapp/api/SendPush?resto_name="+MyUtil.getPref(this,"restaurant_name")+"&order_id="+intent.getStringExtra("order_id")+"&resto_id="+MyUtil.getPref(this,"merchant_id")+"&driver_id="+driverId+"&client_id="+clientId

        Log.d("Push","URL FOR PUSH "+url)

        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                }

                is Result.Success -> {

                    val data = result.get()

                    Log.d("Today", "Orders resp " + data)

                    val obj = JSONObject(MyUtil.correctResponse(data))

                    if (obj.getInt("code") == 1) {

                        handler = Handler()
                        handler!!.postDelayed(Runnable {
                            //Do something after 100ms

                            scheduleDelivery()

                        }, 55000)

                    } else {
                        Toast.makeText(this, "Something is not right!", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }

    }


    public override fun onResume() {
        super.onResume()

        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter("driver-response"))
    }

    // handler for received Intents for the "my-event" event
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if (intent.getBooleanExtra("status",false))
            {
               handler!!.removeCallbacksAndMessages(null)
              //success can go next()
                loading_layout.visibility = View.GONE
                Toast.makeText(this@OrderDetailActivity,"Task successfully assigned to the driver!",Toast.LENGTH_SHORT).show()

            }else
            {
                handler!!.removeCallbacksAndMessages(null)
                scheduleDelivery()
            }

        }
    }

    override fun onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        super.onPause()
    }


    fun sendCustomerPush(status:String,body:String)
    {
        if (clientId!="0")
        {
            var url = "https://fruudo.com/mobileapp/api/SendOrderUpdateToUser?client_id="+clientId+"&order_id="+intent.getStringExtra("order_id")+"&status="+status+"&title=Order Update&body="+body+"&order_id="+intent.getStringExtra("order_id")
            url = url.replace(" ","%20")

            Log.d("UserUpdate","customer push "+url)

            url.httpGet().responseString { request, response, result ->
                //do something with response
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        ex.printStackTrace()
                    }

                    is Result.Success -> {

                        val data = result.get()
                        Log.d("Data", "Informed to customer:: $data")

                    }
                }
            }
        }

    }



}
