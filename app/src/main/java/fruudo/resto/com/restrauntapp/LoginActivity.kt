package fruudo.resto.com.restrauntapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.content_login.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        loginButton.setOnClickListener {

            showError()
            if (!email.text.isEmpty() && !password.text.isEmpty())
            {
               login(email.text.toString(),password.text.toString())
            }

        }

    }

    fun showError()
    {
        if (email.text.isEmpty())
            email_layout.error = "Please enter email"
        else
            email_layout.error = null

        if (password.text.isEmpty())
            password_layout.error = "Please enter email"
        else
            password_layout.error = null

    }


    fun login(username:String, password:String)
    {
        progress.visibility = View.VISIBLE
        loginButton.visibility = View.GONE

        val url = "http://fruudo.com/merchantapp/api/login?username="+username+"&password="+password
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    progress.visibility = View.GONE
                    loginButton.visibility = View.VISIBLE
                }
                is Result.Success -> {

                    progress.visibility = View.GONE
                    loginButton.visibility = View.VISIBLE

                    val data = result.get()

                    Log.d("Response","Res "+data)

                    val obj = JSONObject(data)

                    if (obj.getInt("code")==1)
                    {
                        getDetails(obj.getJSONObject("details").getJSONObject("info").getString("merchant_id"))
                        MyUtil.savePref(this,"token",obj.getJSONObject("details").getString("token"))
                        MyUtil.savePref(this,"username",username)
                        MyUtil.savePref(this,"login",true)
                        MyUtil.savePref(this,"logo",obj.getJSONObject("details").getJSONObject("info").getString("logo"))
                        MyUtil.savePref(this,"restaurant_name",obj.getJSONObject("details").getJSONObject("info").getString("restaurant_name"))
                        MyUtil.savePref(this,"user_type",obj.getJSONObject("details").getString("token"))
                        MyUtil.savePref(this,"merchant_id",obj.getJSONObject("details").getJSONObject("info").getString("merchant_id"))

                        startActivity(Intent(this,MainActivity::class.java))
                        finish()

                    }else
                    {
                        Toast.makeText(this,"Please check your username and password!", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }

    }


    fun getDetails(mt_id:String)
    {
        val url = "https://fruudo.com/merchantapp/api/GetMerchantDetails?merchant_id="+mt_id
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                }
                is Result.Success -> {

                    val data = result.get()

                    Log.d("Response","Res "+data)

                    val obj = JSONObject(data)

                    if (obj.getInt("code")==1)
                    {
                        MyUtil.savePref(this,"lat",obj.getJSONArray("details").getJSONObject(0).getString("latitude"))
                        MyUtil.savePref(this,"lng",obj.getJSONArray("details").getJSONObject(0).getString("lontitude"))

                    }else
                    {
                        Toast.makeText(this,"Please check your username and password!", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }
    }
}
