package fruudo.resto.com.restrauntapp;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.Handler;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d("Remote","Remote_Message "+remoteMessage.getData());

        if (remoteMessage.getData().containsKey("driver_response"))
        {
            inform();
        }else if (remoteMessage.getData().containsKey("reason") && remoteMessage.getData().get("reason").equalsIgnoreCase("new_order"))
        {
            OrderNotification.notify(getApplicationContext(),remoteMessage.getData().get("title"),remoteMessage.getData().get("body"),Integer.parseInt(remoteMessage.getData().get("order_id")));
        }
    }

    private void inform() {
        Intent intent = new Intent("driver-response");
        // add data
        intent.putExtra("message", "data");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onNewToken(String token) {

        if (MyUtil.getBoolPref(getApplicationContext(),"login"))
        saveToken(token);
        else
            MyUtil.savePref(getApplicationContext(),"push_token",token);

    }

    void saveToken(String token)
    {
        Fuel.get("http://fruudo.com/merchantapp/api/UpdatePush?push_token="+token+"&merchant_id="+MyUtil.getPref(getApplicationContext(),"merchant_id")).responseString(new Handler<String>() {
            @Override
            public void failure(Request request, Response response, FuelError error) {
                //do something when it is failure
                error.printStackTrace();
            }

            @Override
            public void success(Request request, Response response, String data) {
                //do something when it is successful
                MyUtil.savePref(getApplicationContext(),"token_saved",true);

            }
        });
    }




}
