package fruudo.resto.com.restrauntapp

data class Item(
        val id: String,
        val order_id: String,
        val client_id: String,
        val item_id: String,
        val item_name: String,
        val order_notes: String,
        val normal_price: String,
        val discounted_price: String,
        val size: String,
        val qty: String,
        val cooking_ref: String,
        val addon: String,
        val ingredients: String,
        val non_taxable: String
)

